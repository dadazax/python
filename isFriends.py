import time
def divisorsum(n):
    return sum([sum({x,n//x}) for x in range(1,int(n**0.5)+1) if n % x ==0])-n

def ds2(n):
    return sum([sum({x,n//x}) for x in range(2,int(n**0.5)+1) if n % x ==0])

def isPerfect(n):
    return {x for x in range(1,n+1) if divisorsum(x) == x}

def amicablePair(n):
    return [(i,divisorsum(i)) for i in range(1,n+1) if i <divisorsum(i) and i == divisorsum(divisorsum(i))]

def betrothedPair(n):
    return [(x,ds2(x)) for x in range(1,n+1) if x < ds2(x) and x == ds2(ds2(x))]
tStart = time.time() #計時開始
print("完美數",isPerfect(100000))
print("好友數",amicablePair(100000))
print("婚約數",betrothedPair(100000))
tEnd = time.time() #計時結束
print(f"總共執行了{(tEnd - tStart):.4f}秒")
