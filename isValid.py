class Solution:
    def isValid(self, s: str) -> bool:
        stack = [] #初始化stack
        for i in s: #把str一個一個列出
            if i in {'[', '(', '{'}: 
                stack.append(i) #如果是左括號 加入stack
            elif i == ']' and stack and stack[-1] == '[':
                stack.pop() #如果是],且stack還存在(不存在stack[-1]會出錯),且stack最後一個為[,移除stack最後一欄
            elif i == ')' and stack and stack[-1] == '(':
                stack.pop() #如果是),且stack還存在(不存在stack[-1]會出錯),且stack最後一個為(,移除stack最後一欄
            elif i == '}' and stack and stack[-1] == '{':
                stack.pop() #如果是},且stack還存在(不存在stack[-1]會出錯),且stack最後一個為{,移除stack最後一欄
            else:
                return False #上述條件不符,則不符合規定
        return not stack #把i in s都跑完後,若stack還有值,仍是失敗,若stack為空,則成功
