class Solution:
    def longestCommonPrefix(self, strs: List[str]) -> str:
        b = ''
        a=list(zip(*strs))
        for i in a:
            if len(set(i)) == 1:
                b += i[0]
            else:
                break
        return b
