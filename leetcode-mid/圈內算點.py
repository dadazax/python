class Solution:
    def isinclude(self,point, querie):
        return True if (abs(point[0] - querie[0]) ** 2 + abs(point[1] - querie[1]) ** 2) ** 0.5 <= querie[2] else False
    def countPoints(self, points: List[List[int]], queries: List[List[int]]) -> List[int]:
        ans = []
        for i in queries:
            a = [1 for j in points if self.isinclude(j, i)]
            ans.append(sum(a))
        return ans
