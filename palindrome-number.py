class Solution:
    def isPalindrome(self, x: int) -> bool:
        x = str(x)
        lens = len(x)
        for z in range(0, lens):
            if x[z] != x[-z-1]:
                return False
            else:
                continue
        return True
