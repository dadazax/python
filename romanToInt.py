class Solution:
    def romanToInt(self, s: str) -> int:
        dic = { 'I':1, 'V':5, 'X':10, 'L':50, 'C':100, 'D':500, 'M':1000,
                'IV':4 ,'IX':9, 'XL':40, 'XC':90, 'CD':400, 'CM':900} #羅馬數字字典
        sum=0 #初始化總計
        lens = len(s) #計算輸入字串S長度
        i = 0 #初始化索引
        while i < lens - 1: #要判斷兩個字符 若i == lens-1的話 s[i+1]會有問題
            tmp = s[i] + s[i + 1] #兩個字符加起來
            if tmp in dic:  #判斷是否在字典內
                sum = sum + dic[tmp] #若是,總計加上
                i = i + 2  #判斷了兩個字,索引+2
            else:
                sum = sum + dic[s[i]] #若不在字典內,則直接加上本身
                i = i + 1 #判斷一個字,索引+1
        if i < lens: #若 i是最後一個位數
            sum = sum + dic[s[i]]  #sum再加上最後一位本身
        return sum
