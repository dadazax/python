from itertools import permutations
def ttoi(tuple):#元祖轉移數字
    #return int(''.join((str(i) for i in tuple)))
    return int(''.join(map(str,tuple)))
def check(t):#判斷是否符合
    return ttoi(t[:5]) + ttoi((t[3],t[5],t[6]))*2 == ttoi(t[7:]+(t[3],t[4]))
#主程式
for p in permutations(range(10),10):
    if check(p):
        print(p)
