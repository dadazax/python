def stair_fast(n):
    ways = [0]* (n+1) #用一個列表記錄爬到第i階階梯共幾種方法
    ways[0] = 1 # 初始條件
    ways[1] = 1 # 初始條件
    for i in range(2,n+1):
        ways[i]=ways[i-1]+ways[i-2]
    print(ways)
    return ways[-1]
print(stair_fast(30))
