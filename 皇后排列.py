def conflict(state, nextX):
    nextY = len(state)
    return any(abs(state[i] - nextX) in (0, nextY - i) for i in range(nextY))

def queens(n, state=()): #n代表總共有幾個皇后 state以元組代表前已經擺好的皇后位置
    if len(state) == n:
        return [()]
    ans = [] # 記錄在state棋子已經放好的狀況下，後續的所有解答
    for pos in range(n):  # 嘗試在下一列中放新的皇后
        if not conflict(state, pos):  #if 這個位置不在前面皇后的攻擊範圍:
            ans += [(pos,)+ result for result in queens(n, state + (pos,))]
            #ans 添加所有來自queens(n, state + (位置,))的解答
    return ans

a=queens(5)
print(f'final{a}')
